package com.s1x6.courierdatabase.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CityDTO
import com.s1x6.courierdatabase.database.FilialDTO
import java.io.Serializable

class CrossAdapter : RecyclerView.Adapter<CrossAdapter.CrossViewHolder>() {

    private val items = ArrayList<Serializable>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = CrossViewHolder(LayoutInflater.from(p0.context).inflate(
        R.layout.cross_item,
        p0,
        false
    ))

    fun setItems(list: List<Serializable>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount() =
        items.size / 2

    override fun onBindViewHolder(p0: CrossViewHolder, p1: Int) {
        val i = p1 * 2
        p0.bind(items[i] as FilialDTO, items[i + 1] as CityDTO)
    }

    class CrossViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvCity = itemView.findViewById<TextView>(R.id.tvCity)
        private val tvAddress = itemView.findViewById<TextView>(R.id.tvAddress)

        fun bind(filialDTO: FilialDTO, cityDTO: CityDTO) {
            tvCity.text = cityDTO.name
            tvAddress.text = filialDTO.address
        }
    }

}
