package com.s1x6.courierdatabase.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.activity.EditCityActivity
import com.s1x6.courierdatabase.database.CityDTO

class CityAdapter : RecyclerView.Adapter<CityAdapter.CityViewHolder>() {
    private val list = ArrayList<CityDTO>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = CityViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.filial_item, p0, false)
    )

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: CityViewHolder, p1: Int) {
        p0.setName(list[p1])
        p0.itemView.setOnClickListener {
            p0.itemView.context.startActivity(
                Intent(p0.itemView.context, EditCityActivity::class.java).apply {
                    putExtra("city", list[p1])
                }
            )
        }
    }

    fun setItems(list: List<CityDTO>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var tvName = itemView.findViewById<TextView>(R.id.tvAddress)

        fun setName(filialDTO: CityDTO) {
            tvName.text = filialDTO.name
        }
    }
}