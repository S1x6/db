package com.s1x6.courierdatabase.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.StatisticDTO

class StatisticsAdapter : RecyclerView.Adapter<StatisticsAdapter.UserViewHolder>() {

    private val list = ArrayList<StatisticDTO>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = UserViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.stat_item, p0, false)
    )

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: UserViewHolder, p1: Int) {
        p0.setFields(list[p1])
    }

    fun setItems(list: List<StatisticDTO>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvName = itemView.findViewById<TextView>(R.id.tvUserName)
        private val tvOrders = itemView.findViewById<TextView>(R.id.tvOrders)

        fun setFields(statisticDTO: StatisticDTO) {
            tvName.text = statisticDTO.userName
            tvOrders.text = statisticDTO.ordersNum.toString()
        }

    }

}
