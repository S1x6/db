package com.s1x6.courierdatabase.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CompanyDTO

class CompaniesAdapter(private val listener: OnCompanyItemClickListener) : RecyclerView.Adapter<CompaniesAdapter.CompanyViewHolder>() {

    private val list: MutableList<CompanyDTO> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = CompanyViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.filial_item, p0, false)
    )

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: CompanyViewHolder, p1: Int) {
        p0.bind(list[p1])
        p0.itemView.setOnClickListener{
            listener.performAction(list[p1])
        }
    }

    fun setItems(list: List<CompanyDTO>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class CompanyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvName = itemView.findViewById<TextView>(R.id.tvAddress)

        fun bind(companyDTO: CompanyDTO) {
            tvName.text = companyDTO.name
        }

    }

    interface OnCompanyItemClickListener {
        fun performAction(companyDTO: CompanyDTO)
    }

}
