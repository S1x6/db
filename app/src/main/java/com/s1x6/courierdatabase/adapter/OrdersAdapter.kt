package com.s1x6.courierdatabase.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.activity.EditOrderActivity
import com.s1x6.courierdatabase.database.OrderDTO
import java.util.*
import kotlin.collections.ArrayList

class OrdersAdapter : RecyclerView.Adapter<OrdersAdapter.OrderViewHolder>() {

    private val orders = ArrayList<OrderDTO>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) =
        OrderViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.order_item,
                p0,
                false
            )
        )

    override fun getItemCount(): Int {
        return orders.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(p0: OrderViewHolder, p1: Int) {
        val order = orders[p1]
        p0.addressTv.text = order.address
        p0.distanceTv.text = order.distance.toString()
        p0.moneyTv.text = order.money.toString()
        order.filial?.let { p0.filialAddressTv.text = it.address }
        val takeDate = Date(order.timeTake * 1000)
        val deliveryDate = Date(order.timeDelivery * 1000)
        p0.statusTv.text = when (order.status) {
            0 -> "В процессе"
            else -> "Доставлен"
        }
        p0.timeTakeTv.text = "${if (takeDate.hours < 10) {
            "0"
        } else {
            ""
        }}${takeDate.hours}:${if (takeDate.minutes < 10) {
            "0"
        } else {
            ""
        }}${takeDate.minutes}"
        p0.timeDelTv.text = "${if (deliveryDate.hours < 10) {
            "0"
        } else {
            ""
        }}${deliveryDate.hours}:${if (deliveryDate.minutes < 10) {
            "0"
        } else {
            ""
        }}${deliveryDate.minutes}"
        p0.itemView.setOnClickListener {
            it.context.startActivity(Intent(it.context, EditOrderActivity::class.java).apply {
                putExtra(
                    "dto",
                    orders[p1]
                )
            })
        }
    }

    fun setOrders(orders: List<OrderDTO>) {
        this.orders.clear()
        this.orders.addAll(orders)
    }

    fun addOrders(orders: List<OrderDTO>) {
        this.orders.addAll(orders)
        notifyDataSetChanged()
    }

    class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val addressTv: TextView = itemView.findViewById(R.id.addressTo)
        val statusTv: TextView = itemView.findViewById(R.id.status)
        val moneyTv: TextView = itemView.findViewById(R.id.companyMoney)
        val distanceTv: TextView = itemView.findViewById(R.id.deliveryMoney)
        val filialAddressTv: TextView = itemView.findViewById(R.id.addressFrom)
        val timeTakeTv: TextView = itemView.findViewById(R.id.takeTime)
        val timeDelTv: TextView = itemView.findViewById(R.id.deliveryTime)
    }
}