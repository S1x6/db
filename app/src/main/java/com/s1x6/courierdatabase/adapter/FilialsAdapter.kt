package com.s1x6.courierdatabase.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.activity.EditFilialActivity
import com.s1x6.courierdatabase.database.FilialDTO

class FilialsAdapter : RecyclerView.Adapter<FilialsAdapter.FilialViewHolder>() {

    private val list = ArrayList<FilialDTO>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = FilialViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.filial_item, p0, false)
    )

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: FilialViewHolder, p1: Int) {
        p0.setAddress(list[p1])
        p0.itemView.setOnClickListener { p0.itemView.context.startActivity(
            Intent(p0.itemView.context, EditFilialActivity::class.java).apply {
                putExtra("filial", list[p1])
            }
        ) }
    }

    fun setItems(list: List<FilialDTO>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    class FilialViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var tvAddress = itemView.findViewById<TextView>(R.id.tvAddress)

        fun setAddress(filialDTO: FilialDTO) {
            tvAddress.text = filialDTO.address
        }
    }
}
