package com.s1x6.courierdatabase.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.activity.*

class MenuRecyclerAdapter(val context: Context) : RecyclerView.Adapter<MenuRecyclerAdapter.MyViewHolder>() {

    private var items = mutableListOf("Заказы", "Транзакции")

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) =
        MyViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.menu_textview_item, p0, false))

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.setText(items[p1])
        when (p1) {
            0 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, OrdersActivity::class.java))
            }
            1 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, TransactionsActivity::class.java))
            }
            2 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, FilialsActivity::class.java))
            }
            3 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, StatisticsActivity::class.java))
            }
            4 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, EditCompanyActivity::class.java))
            }
            5 -> p0.itemView.setOnClickListener {
                context.startActivity(Intent(context, CrossActivity::class.java))
            }
        }
    }

    fun showFilials(show: Boolean) {
        items = if (show) {
            mutableListOf("Заказы", "Транзакции", "Филиалы", "Статистика подчиненных", "Редактировать компанию", "Cross join")
        } else {
            mutableListOf("Заказы", "Транзакции")
        }
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val textView = itemView.findViewById<TextView>(R.id.textView)

        fun setText(text: String) {
            textView.text = text
        }
    }
}