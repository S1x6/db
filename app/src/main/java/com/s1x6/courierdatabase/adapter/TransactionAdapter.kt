package com.s1x6.courierdatabase.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.activity.EditTransactionActivity
import com.s1x6.courierdatabase.database.TransactionDTO
import java.util.*
import kotlin.collections.ArrayList

class TransactionAdapter : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    private val list = ArrayList<TransactionDTO>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = TransactionViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.transaction_item, p0, false)
    )

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: TransactionViewHolder, p1: Int) {
        p0.setFields(list[p1])
        p0.itemView.setOnClickListener {
            it.context.startActivity(Intent(it.context, EditTransactionActivity::class.java).apply { putExtra("dto", list[p1]) })
        }
    }

    fun setItems(list: List<TransactionDTO>) {
        this.list.clear()
        this.list.addAll(list)
    }

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var tvSum = itemView.findViewById<TextView>(R.id.tvSum)
        private var tvTime = itemView.findViewById<TextView>(R.id.tvTime)
        private var tvBalance = itemView.findViewById<TextView>(R.id.tvBalance)

        @SuppressLint("SetTextI18n")
        fun setFields(transactionDTO: TransactionDTO) {
            tvSum.text = "${transactionDTO.sum} р"
            val date = Date(transactionDTO.time*1000)
            tvTime.text = "${if (date.hours < 10) {
                "0"
            } else {
                ""
            }}${date.hours}:${if (date.minutes < 10) {
                "0"
            } else {
                ""
            }}${date.minutes}"
            tvBalance.text = "Баланс: ${transactionDTO.balanceAfter} р"
        }
    }
}