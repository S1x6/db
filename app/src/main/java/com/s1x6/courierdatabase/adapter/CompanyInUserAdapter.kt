package com.s1x6.courierdatabase.adapter

import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.WorkRelationDAO


class CompanyInUserAdapter : RecyclerView.Adapter<CompaniesAdapter.CompanyViewHolder>() {

    private val list: MutableList<CompanyDTO> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int) = CompaniesAdapter.CompanyViewHolder(
        LayoutInflater.from(p0.context).inflate(R.layout.filial_item, p0, false)
    )

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: CompaniesAdapter.CompanyViewHolder, p1: Int) {
        p0.bind(list[p1])
        p0.itemView.setOnClickListener {
            val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        WorkRelationDAO.fireUserFromCompany(
                            it.context,
                            SharedPreferencesHelper.getValue(it.context, SharedPreferencesHelper.userId)?.toLong(),
                            list[p1].id
                        )
                        list.removeAt(p1)
                        notifyDataSetChanged()
                    }

                    DialogInterface.BUTTON_NEGATIVE -> {
                    }
                }
            }
            val builder = AlertDialog.Builder(it.context)
            builder.setMessage("Уволиться из компании?").setPositiveButton("Да", dialogClickListener)
                .setNegativeButton("Нет", dialogClickListener).show()
        }
    }

    fun setItems(list: List<CompanyDTO>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}
