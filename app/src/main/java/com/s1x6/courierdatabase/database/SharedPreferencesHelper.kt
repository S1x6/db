package com.s1x6.courierdatabase.database

import android.content.Context

private const val NAME = "preferences"
object SharedPreferencesHelper {

    const val userId = "userID"
    const val companyId = "companyID"

    fun getValue(context: Context, key: String): String? {
        val pref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
        return pref.getString(key, "")
    }

    fun saveValue(context: Context, key: String, value: String) {
        val pref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
        val e = pref.edit()
        e.putString(key, value)
        e.apply()
    }

}
