package com.s1x6.courierdatabase.database.dao

import android.content.Context
import android.widget.Toast
import com.s1x6.courierdatabase.database.*
import com.s1x6.courierdatabase.dialog.OrdersSortDialog
import java.io.Serializable
import java.lang.Exception

object OrderDAO : DAO {
    override fun getAll(context: Context): List<OrderDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val companyId =
            SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        val c = db.rawQuery(
            "SELECT * FROM `Order` o JOIN Filial f ON o.FilialID = f.ID WHERE f.CompanyID = $companyId",
            null
        )
        val list = ArrayList<OrderDTO>()
        while (c.moveToNext()) {
            val filial = FilialDTO(
                c.getLong(9),
                c.getString(10),
                c.getLong(11),
                c.getLong(12)
            )
            list.add(
                OrderDTO(
                    c.getLong(0),
                    c.getLong(1),
                    c.getLong(2),
                    c.getInt(3),
                    c.getLong(4),
                    c.getLong(7),
                    c.getInt(6),
                    c.getString(8),
                    filial,
                    c.getInt(5)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun getSortedPart(
        context: Context,
        offset: Int,
        limit: Int,
        sortField: OrdersSortDialog.SortField,
        sortDir: OrdersSortDialog.SortDir,
        filterOrder: OrderDTO
    ): List<OrderDTO> {
        val helper = DBHelper(context)
        val companyId =
            SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        val db = helper.readableDatabase
        var whereClause = ""
        if (filterOrder.id > 0) {
            whereClause += " AND o.ID = ${filterOrder.id}"
        }
        if (filterOrder.distance > 0) {
            whereClause += " AND Distance = ${filterOrder.distance}"
        }
        if (filterOrder.money >= 0) {
            whereClause += " AND Money = ${filterOrder.money}"
        }
        if (filterOrder.filialId > 0) {
            whereClause += " AND o.FilialID = ${filterOrder.filialId}"
        }
        if (filterOrder.userId > 0) {
            whereClause += " AND o.UserID = ${filterOrder.userId}"
        }
        if (filterOrder.status >= 0) {
            whereClause += " AND Status = ${filterOrder.status}"
        }
        if (filterOrder.address.isNotEmpty()) {
            whereClause += " AND o.Address LIKE \'%${filterOrder.address}%\'"
        }
        val c = db.rawQuery(
            "SELECT * FROM `Order` o JOIN Filial f ON o.FilialID = f.ID WHERE f.CompanyID = $companyId $whereClause ORDER BY ${sortField.name} ${sortDir.name} LIMIT $offset, $limit",
            null
        )
        val list = ArrayList<OrderDTO>()
        while (c.moveToNext()) {
            val filial = FilialDTO(
                c.getLong(9),
                c.getString(10),
                c.getLong(11),
                c.getLong(12)
            )
            list.add(
                OrderDTO(
                    c.getLong(0),
                    c.getLong(1),
                    c.getLong(2),
                    c.getInt(3),
                    c.getLong(4),
                    c.getLong(7),
                    c.getInt(6),
                    c.getString(8),
                    filial,
                    c.getInt(5)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun finishOrders(context: Context) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val companyId =
            SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        var balance = CompanyDAO.findById(context, companyId)?.balance ?: 0
        val time = System.currentTimeMillis() / 1000
        val sql =
            "SELECT * FROM `Order` o JOIN Filial f ON o.FilialID = f.ID WHERE TimeDelivery < $time AND Status = 0 AND f.CompanyID = $companyId"
        val c =
            db.rawQuery(
                sql,
                null
            )
        val list = ArrayList<OrderDTO>()
        while (c.moveToNext()) {
            val filial = FilialDTO(
                c.getLong(9),
                c.getString(10),
                c.getLong(11),
                c.getLong(12)
            )
            list.add(
                OrderDTO(
                    c.getLong(0),
                    c.getLong(1),
                    c.getLong(2),
                    c.getInt(3),
                    c.getLong(4),
                    c.getLong(7),
                    c.getInt(6),
                    c.getString(8),
                    filial,
                    c.getInt(5)
                )
            )
        }
        c.close()
        //Toast.makeText(context, list.size.toString(), Toast.LENGTH_SHORT).show()
        db.beginTransaction()
        try {
            list.forEach {
                balance += it.money
                db.execSQL("UPDATE `Order` SET Status = 1 WHERE ID = ${it.id}")
                db.execSQL("UPDATE `Company` SET Balance = Balance + ${it.money} WHERE ID = $companyId")
                db.execSQL(
                    "INSERT INTO `Transaction`(Sum, BalanceAfter, Time, OrderID, CompanyID) VALUES(" +
                            "${it.money}, $balance, $time, ${it.id}, $companyId)"
                )
            }
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
        }
    }

    override fun findById(context: Context, id: Long): Serializable? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val orderDTO = dto as OrderDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL(
                "INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                        " VALUES(${orderDTO.timeTake}, ${orderDTO.timeDelivery}, ${orderDTO.status}, ${orderDTO.userId}, ${orderDTO.distance}" +
                        ", ${orderDTO.money}, ${orderDTO.filialId}, \"${orderDTO.address}\")"
            )
            val c = db.rawQuery("SELECT max(ID) FROM `Order`", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    fun update(context: Context, orderDTO: OrderDTO) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        db.execSQL(
            "UPDATE `Order` SET TimeTake = ${orderDTO.timeTake}, TimeDelivery = ${orderDTO.timeDelivery}, " +
                    "Status = ${orderDTO.status}, UserID = ${orderDTO.userId}, Distance = ${orderDTO.distance}," +
                    "Money = ${orderDTO.money}, FilialID = ${orderDTO.filialId}, Address = \'${orderDTO.address}\' WHERE ID = ${orderDTO.id}"
        )
        db.close()
        helper.close()
    }

    fun delete(context: Context, id: Long): Boolean {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        var a = true
        db.beginTransaction()
        try {
            db.execSQL("PRAGMA foreign_keys=ON")
            db.execSQL("DELETE FROM `Order` WHERE ID = $id")
            db.setTransactionSuccessful()
        } catch (e: Exception) {
            a = false
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a
        }
    }
}