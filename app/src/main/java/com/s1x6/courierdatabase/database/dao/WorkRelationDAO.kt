package com.s1x6.courierdatabase.database.dao

import android.content.Context
import com.s1x6.courierdatabase.database.*
import java.io.Serializable

object WorkRelationDAO : DAO {
    override fun getAll(context: Context): List<Serializable> {
        throw NotImplementedError()
    }

    override fun findById(context: Context, id: Long): Serializable? {
        throw NotImplementedError()
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val workRelationDTO = dto as WorkRelationDTO
        db.execSQL("INSERT INTO Works VALUES(\"${workRelationDTO.userId}\", ${workRelationDTO.companyId})")
        db.close()
        helper.close()
        return 0
    }

    fun findCompanies(context: Context, userDto: UserDTO): List<CompanyDTO> {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val list = ArrayList<CompanyDTO>()
        val c = db.rawQuery("SELECT * FROM Works w JOIN Company c ON w.CompanyID = c.ID WHERE w.UserID = ${userDto.id}", null)
        while (c.moveToNext()) {
            list.add(
                CompanyDTO(
                    c.getLong(2),
                    c.getString(3),
                    c.getInt(4),
                    c.getLong(5)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun fireUserFromCompany(context: Context, userId: Long?, companyId: Long) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        db.execSQL("DELETE FROM Works WHERE UserID = $userId AND CompanyID = $companyId")
        db.close()
        helper.close()
    }
}