package com.s1x6.courierdatabase.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

private const val SQL_CREATE_TABLE1 = "CREATE TABLE User\n" +
        "(\n" +
        "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
        "  Name VARCHAR(64) NOT NULL,\n" +
        "  Login VARCHAR(64) NOT NULL,\n" +
        "  Password VARCHAR(64) NOT NULL" +
        ");"
private const val SQL_CREATE_TABLE2 = "CREATE TABLE Company\n" +
        "(\n" +
        "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
        "  Name VARCHAR(64) UNIQUE,\n" +
        "  Balance INT NOT NULL,\n" +
        "  OwnerID INT,\n" +
        "  FOREIGN KEY (OwnerID) REFERENCES User(ID)\n" +
        ");"
private const val SQL_CREATE_TABLE3 =
    "CREATE TABLE `Order`\n" +
            "(\n" +
            "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "  TimeTake INT NOT NULL,\n" +
            "  TimeDelivery INT NOT NULL,\n" +
            "  Status VARCHAR(64) NOT NULL,\n" +
            "  UserID INT NOT NULL,\n" +
            "  Distance INT NOT NULL, \n" +
            "  Money INT NOT NULL, \n" +
            "  FilialID INT NOT NULL, \n" +
            "  Address VARCHAR(64) NOT NULL, \n" +
            "  CHECK (TimeTake < TimeDelivery), \n" +
            "  FOREIGN KEY (UserID) REFERENCES User(ID),\n" +
            "  FOREIGN KEY (FilialID) REFERENCES Filial(ID)\n" +
            ");"
private const val SQL_CREATE_TABLE4 =
    "CREATE TABLE `Transaction`\n" +
            "(\n" +
            "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "  Sum INT NOT NULL,\n" +
            "  BalanceAfter INT NOT NULL,\n" +
            "  Time INT NOT NULL,\n" +
            "  OrderID INT NOT NULL,\n" +
            "  CompanyID INT NOT NULL,\n" +
            "  FOREIGN KEY (OrderID) REFERENCES `Order`(ID),\n" +
            "  FOREIGN KEY (CompanyID) REFERENCES Company(ID)\n" +
            ");"
private const val SQL_CREATE_TABLE5 =
    "CREATE TABLE City\n" +
            "(\n" +
            "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "  Name VARCHAR(64) NOT NULL, \n" +
            "  DeliveryPrice INT NOT NULL\n" +
            ");"
private const val SQL_CREATE_TABLE6 =
    "CREATE TABLE Works\n" +
            "(\n" +
            "  UserID INT NOT NULL,\n" +
            "  CompanyID INT NOT NULL,\n" +
            "  PRIMARY KEY (UserID, CompanyID),\n" +
            "  FOREIGN KEY (UserID) REFERENCES User(ID),\n" +
            "  FOREIGN KEY (CompanyID) REFERENCES Company(ID)\n" +
            ");"
private const val SQL_CREATE_TABLE7 =
    "CREATE TABLE Filial\n" +
            "(\n" +
            "  ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "  Address VARCHAR(128) NOT NULL,\n" +
            "  CompanyID INT NOT NULL,\n" +
            "  CityID INT NOT NULL,\n" +
            "  FOREIGN KEY (CompanyID) REFERENCES Company(ID),\n" +
            "  FOREIGN KEY (CityID) REFERENCES City(ID)\n" +
            ");\n"

private const val SQL_DELETE_TABLE1 = "DROP TABLE IF EXISTS `User`; "
private const val SQL_DELETE_TABLE2 = "DROP TABLE IF EXISTS `Company`; "
private const val SQL_DELETE_TABLE3 = "DROP TABLE IF EXISTS `Order`; "
private const val SQL_DELETE_TABLE4 = "DROP TABLE IF EXISTS `Transaction`; "
private const val SQL_DELETE_TABLE5 = "DROP TABLE IF EXISTS `City`; "
private const val SQL_DELETE_TABLE6 = "DROP TABLE IF EXISTS `Works`; "
private const val SQL_DELETE_TABLE7 = "DROP TABLE IF EXISTS `Filial`;"

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {

        db.execSQL(SQL_CREATE_TABLE1)
        db.execSQL(SQL_CREATE_TABLE2)
        db.execSQL(SQL_CREATE_TABLE5)
        db.execSQL(SQL_CREATE_TABLE7)
        db.execSQL(SQL_CREATE_TABLE3)
        db.execSQL(SQL_CREATE_TABLE4)
        db.execSQL(SQL_CREATE_TABLE6)
        fillDB(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TABLE1)
        db.execSQL(SQL_DELETE_TABLE2)
        db.execSQL(SQL_DELETE_TABLE3)
        db.execSQL(SQL_DELETE_TABLE4)
        db.execSQL(SQL_DELETE_TABLE5)
        db.execSQL(SQL_DELETE_TABLE6)
        db.execSQL(SQL_DELETE_TABLE7)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 12
        const val DATABASE_NAME = "OrderDB.db"
    }

    private fun fillDB(db: SQLiteDatabase) {
        db.beginTransaction()
        db.execSQL("INSERT INTO City(Name, DeliveryPrice) VALUES(\"Новосибирск\", 120)")
        db.execSQL("INSERT INTO City(Name, DeliveryPrice) VALUES(\"Москва\", 250)")
        db.execSQL("INSERT INTO City(Name, DeliveryPrice) VALUES(\"Томск\", 100)")
        db.execSQL("INSERT INTO User(Name, Login, Password) VALUES(\"Андрей Белозеров\", \"Andrey\", \"0000\")")
        db.execSQL("INSERT INTO User(Name, Login, Password) VALUES(\"Василий Петров\", \"Vasya\", \"0000\")")
        db.execSQL("INSERT INTO User(Name, Login, Password) VALUES(\"Дмитрий Звон\", \"Dima\", \"0000\")")
        db.execSQL("INSERT INTO Company(Name, Balance, OwnerID) VALUES(\"New York Pizza\", 0, 1)")
        db.execSQL("INSERT INTO Company(Name, Balance, OwnerID) VALUES(\"СушиMake\", 0, 2)")
        db.execSQL("INSERT INTO Works(CompanyID, UserID) VALUES(1,1)")
        db.execSQL("INSERT INTO Works(CompanyID, UserID) VALUES(2,1)")
        db.execSQL("INSERT INTO Works(CompanyID, UserID) VALUES(2,2)")
        db.execSQL("INSERT INTO Works(CompanyID, UserID) VALUES(1,3)")
        db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"Пирогова 1\", 1, 1)")
        db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"Красный проспект 14\", 1, 1)")
        db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"Ленина 45/1\", 1, 3)")
        db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"Красная площадь\", 2, 2)")
        db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"Новосибирская 7\", 2, 1)")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1557980022, 1557983022, 0, 1, 30, 3600, 1,\"Новосибирская 15\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1557960022, 1557981022, 0, 1, 4, 480, 2,\"Красный проспект 6, кв. 45\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1558060022, 1558081422, 0, 1, 5, 500, 3,\"Геодезическая 33, кв. 11\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1558060022, 1558081422, 0, 1, 3, 750, 4,\"Охотный ряд\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1555060022, 1555081422, 0, 1, 4, 1000, 4,\"Преображенская 8\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1557060022, 1557091320, 0, 2, 2, 240, 5,\"Никитина 3\")")
        db.execSQL("INSERT INTO `Order`(TimeTake, TimeDelivery, Status, UserID, Distance, Money, FilialID, Address)" +
                " VALUES(1559060022, 1559091320, 0, 2, 3, 360, 5,\"Никитина 666\")")
        db.setTransactionSuccessful()
        db.endTransaction()
    }
}