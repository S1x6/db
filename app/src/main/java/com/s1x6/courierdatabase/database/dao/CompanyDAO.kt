package com.s1x6.courierdatabase.database.dao

import android.content.Context
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.DBHelper
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.UserDTO
import java.io.Serializable
import java.lang.Exception

object CompanyDAO : DAO {

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val companyDTO = dto as CompanyDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL("INSERT INTO Company(Name, Balance, OwnerID) VALUES(\"${companyDTO.name}\", ${companyDTO.balance}, ${companyDTO.ownerId})")
            val c = db.rawQuery("SELECT max(ID) FROM Company", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    override fun getAll(context: Context): List<CompanyDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM Company", null)
        val list = ArrayList<CompanyDTO>()
        while (c.moveToNext()) {
            list.add(
                CompanyDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getInt(2),
                    c.getLong(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    override fun findById(context: Context, id: Long): CompanyDTO? {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM Company WHERE id = ?", arrayOf(id.toString()))
        val dto: CompanyDTO?
        if (c.moveToNext()) {
            dto = CompanyDTO(c.getLong(0), c.getString(1), c.getInt(2), c.getLong(3))
        } else {
            dto = null
        }
        c.close()
        db.close()
        helper.close()
        return dto
    }

    fun getUsersCompanies(context: Context): List<CompanyDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val userID = SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.userId)?.toLongOrNull() ?: 0L
        val c = db.rawQuery("SELECT * FROM Company c JOIN Works w ON c.ID = w.CompanyID WHERE w.UserID = $userID", null)
        val list = ArrayList<CompanyDTO>()
        while (c.moveToNext()) {
            list.add(
                CompanyDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getInt(2),
                    c.getLong(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun getRestCompanies(context: Context): List<CompanyDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val userID = SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.userId)?.toLongOrNull() ?: 0L
        val c = db.rawQuery(
            "SELECT * FROM Company c JOIN Works w ON c.ID = w.CompanyID WHERE Name NOT IN " +
                    "(SELECT Name FROM Company c JOIN Works w ON c.ID = w.CompanyID WHERE w.UserID = $userID)", null
        )
        val list = ArrayList<CompanyDTO>()
        while (c.moveToNext()) {
            list.add(
                CompanyDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getInt(2),
                    c.getLong(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun update(context: Context, dto: CompanyDTO) {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        db.execSQL(
            "UPDATE Company SET Name = \'${dto.name}\', Balance = ${dto.balance} WHERE ID = ${dto.id}"
        )
        db.close()
        helper.close()
    }

}