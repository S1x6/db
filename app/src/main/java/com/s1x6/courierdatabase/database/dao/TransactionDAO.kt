package com.s1x6.courierdatabase.database.dao

import android.content.Context
import com.s1x6.courierdatabase.database.DBHelper
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.TransactionDTO
import java.io.Serializable
import java.lang.Exception

object TransactionDAO : DAO {
    override fun getAll(context: Context): List<TransactionDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM `Transaction`", null)
        val list = ArrayList<TransactionDTO>()
        while (c.moveToNext()) {
            list.add(
                TransactionDTO(
                    c.getLong(0),
                    c.getInt(1),
                    c.getInt(2),
                    c.getLong(3),
                    c.getLong(5),
                    c.getLong(4)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun getPart(context: Context, offset: Int, limit: Int): List<TransactionDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val companyId =
            SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        val c = db.rawQuery("SELECT * FROM `Transaction` WHERE CompanyID = $companyId LIMIT $offset, $limit", null)
        val list = ArrayList<TransactionDTO>()
        while (c.moveToNext()) {
            list.add(
                TransactionDTO(
                    c.getLong(0),
                    c.getInt(1),
                    c.getInt(2),
                    c.getLong(3),
                    c.getLong(5),
                    c.getLong(4)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    override fun findById(context: Context, id: Long): Serializable? {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM `Transaction` WHERE ID = $id", null)
        c.moveToNext()
        val t = TransactionDTO(
            c.getLong(0),
            c.getInt(1),
            c.getInt(2),
            c.getLong(3),
            c.getLong(4),
            c.getLong(5)
        )
        c.close()
        db.close()
        helper.close()
        return t
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val transactionDTO = dto as TransactionDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL(
                "INSERT INTO `Transaction`(Sum, BalanceAfter, Time, OrderID, CompanyID)" +
                        " VALUES(${transactionDTO.sum}, ${transactionDTO.balanceAfter}, ${transactionDTO.time}, ${transactionDTO.orderId}," +
                        " ${transactionDTO.companyId})"
            )
            db.execSQL("UPDATE `Company` SET Balance = Balance + ${transactionDTO.sum} WHERE ID = ${transactionDTO.companyId}")
            val c = db.rawQuery("SELECT max(ID) FROM `Transaction`", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    fun update(context: Context, dto: TransactionDTO) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val old = findById(context, dto.id) as TransactionDTO
        db.beginTransaction()
        try {
            db.execSQL(
                "UPDATE `Transaction` SET Sum = ${dto.sum}, BalanceAfter = ${dto.balanceAfter}, Time = ${dto.time}, CompanyID = ${dto.companyId}, " +
                        " OrderID = ${dto.orderId} WHERE ID = ${dto.id}"
            )
            if (old.companyId == dto.companyId) {
                db.execSQL("UPDATE `Company` SET Balance = Balance - ${old.sum}+ ${dto.sum} WHERE ID = ${dto.companyId}")
            } else {
                db.execSQL("UPDATE `Company` SET Balance = Balance - ${old.sum} WHERE ID = ${old.companyId}")
                db.execSQL("UPDATE `Company` SET Balance = Balance + ${dto.sum} WHERE ID = ${dto.companyId}")
            }
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
        }
    }

    fun delete(context: Context, id: Long) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val old = findById(context, id) as TransactionDTO
        db.beginTransaction()
        try {
            db.execSQL(
                "DELETE FROM `Transaction` WHERE ID = $id"
            )
            db.execSQL("UPDATE `Company` SET Balance = Balance - ${old.sum} WHERE ID = ${old.companyId}")
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
        }
    }
}