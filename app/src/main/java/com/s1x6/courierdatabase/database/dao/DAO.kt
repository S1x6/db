package com.s1x6.courierdatabase.database.dao

import android.content.Context
import java.io.Serializable

interface DAO {
    fun getAll(context: Context): List<Serializable>
    fun findById(context: Context, id:Long): Serializable?
    fun add(context:Context, dto:Serializable): Long
}