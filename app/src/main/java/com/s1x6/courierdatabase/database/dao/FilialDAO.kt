package com.s1x6.courierdatabase.database.dao

import android.content.Context
import com.s1x6.courierdatabase.database.CityDTO
import com.s1x6.courierdatabase.database.DBHelper
import com.s1x6.courierdatabase.database.FilialDTO
import java.io.Serializable

object FilialDAO : DAO {
    override fun getAll(context: Context): List<FilialDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM Filial", null)
        val list = ArrayList<FilialDTO>()
        while (c.moveToNext()) {
            list.add(
                FilialDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getLong(2),
                    c.getLong(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    override fun findById(context: Context, id: Long): Serializable? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val filialDTO = dto as FilialDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL("INSERT INTO Filial(Address, CompanyID, CityID) VALUES(\"${filialDTO.address}\", ${filialDTO.companyId}, ${filialDTO.cityId})")
            val c = db.rawQuery("SELECT max(ID) FROM Filial", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: java.lang.Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    fun getCompanyFilials(context: Context, companyId: Long): List<FilialDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM Filial WHERE CompanyID = $companyId", null)
        val list = ArrayList<FilialDTO>()
        while (c.moveToNext()) {
            list.add(
                FilialDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getLong(2),
                    c.getLong(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun getPrice(context: Context, filialId: Long): Int {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery(
            "SELECT c.DeliveryPrice FROM Filial f JOIN City c ON f.CityID = c.ID WHERE f.ID = $filialId",
            null
        )
        c.moveToNext()
        val price = c.getInt(0)
        c.close()
        db.close()
        helper.close()
        return price
    }

    fun update(context: Context, dto: FilialDTO) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        db.beginTransaction()
        try {
            db.execSQL(
                "UPDATE Filial SET CityID = ${dto.cityId}, CompanyID = ${dto.companyId}, Address = \'${dto.address}\' WHERE ID = ${dto.id}"
            )
            db.setTransactionSuccessful()
        } catch (e: java.lang.Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
        }
    }

    fun delete(context: Context, filialDTO: FilialDTO): Boolean {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        var a = true
        db.beginTransaction()
        try {
            db.execSQL("PRAGMA foreign_keys=ON")
            db.execSQL(
                "DELETE FROM Filial WHERE ID = ${filialDTO.id}"
            )
            db.setTransactionSuccessful()
        } catch (e: Exception) {
            a = false
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a
        }
    }

    fun cross(context: Context): List<Serializable> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val list = ArrayList<Serializable>()
        val c = db.rawQuery("SELECT * FROM Filial f CROSS JOIN City c", null)
        while (c.moveToNext()) {
            list.add(FilialDTO(
                c.getLong(0),
                c.getString(1),
                c.getLong(2),
                c.getLong(3)
            ))
            list.add(
                CityDTO(
                    c.getLong(4),
                    c.getString(5),
                    c.getInt(6)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }
}