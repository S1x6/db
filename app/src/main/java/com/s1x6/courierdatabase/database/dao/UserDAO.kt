package com.s1x6.courierdatabase.database.dao

import android.content.Context
import com.s1x6.courierdatabase.database.*
import java.io.Serializable

object UserDAO : DAO {
    override fun getAll(context: Context): List<UserDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM User", null)
        val list = ArrayList<UserDTO>()
        while (c.moveToNext()) {
            list.add(
                UserDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getString(2),
                    c.getString(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    override fun findById(context: Context, id: Long): UserDTO? {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM User WHERE id = ?", arrayOf(id.toString()))
        val dto: UserDTO?
        if (c.moveToNext()) {
            dto = UserDTO(c.getLong(0), c.getString(1), c.getString(2), c.getString(3))
        } else {
            dto = null
        }
        c.close()
        db.close()
        helper.close()
        return dto
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val userDTO = dto as UserDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL("INSERT INTO User(Name, Login, Password) VALUES(\"${userDTO.name}\", \"${userDTO.login}\", \"${userDTO.password}\")")
            val c = db.rawQuery("SELECT max(ID) FROM User", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: java.lang.Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    fun getByLogin(context: Context, login: String, password: String): UserDTO? {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val c = db.rawQuery("SELECT * FROM User WHERE Login = \"$login\" AND Password = \"$password\"", null)
        var a: UserDTO? = null
        if (c.moveToNext()) {
            a = UserDTO(c.getLong(0), c.getString(1), c.getString(2), c.getString(3))
        }
        c.close()
        db.close()
        helper.close()
        return a
    }

    fun getCompanyOwner(context: Context, companyDTO: CompanyDTO): UserDTO? {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val c = db.rawQuery(
            "SELECT * FROM User u JOIN Company c ON u.ID = c.OwnerID WHERE c.ID = \"${companyDTO.id}\"",
            null
        )
        var a: UserDTO? = null
        if (c.moveToNext()) {
            a = UserDTO(c.getLong(0), c.getString(1), c.getString(2), c.getString(3))
        }
        c.close()
        db.close()
        helper.close()
        return a
    }

    fun getStatistic(context: Context): List<StatisticDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val companyId =
            SharedPreferencesHelper.getValue(context, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        val c = db.rawQuery(
            "SELECT Name, OrdersNum\n" +
                    "FROM\n" +
                    "  (SELECT u.Name,\n" +
                    "          COUNT(o.ID) OrdersNum\n" +
                    "   FROM USER u\n" +
                    "   JOIN Works w ON u.ID = w.UserID\n" +
                    "   LEFT JOIN `Order` o ON o.UserID = u.ID\n" +
                    "   LEFT JOIN Filial f ON f.ID = o.FilialID\n" +
                    "   WHERE w.CompanyID = $companyId\n" +
                    "     AND f.CompanyID = $companyId\n" +
                    "   GROUP BY o.Status,\n" +
                    "            u.ID\n" +
                    "   HAVING o.Status = 1\n" +
                    "   UNION SELECT *\n" +
                    "   FROM\n" +
                    "     (SELECT u.Name,\n" +
                    "             COUNT(o.ID) OrdersNum\n" +
                    "      FROM USER u\n" +
                    "      JOIN Works w ON u.ID = w.UserID\n" +
                    "      LEFT JOIN `Order` o ON o.UserID = u.ID\n" +
                    "      LEFT JOIN Filial f ON f.ID = o.FilialID\n" +
                    "      WHERE w.CompanyID = $companyId\n" +
                    "        AND (f.CompanyID = $companyId\n" +
                    "        OR f.CompanyID IS NULL)\n" +
                    "      GROUP BY u.ID) s\n" +
                    "   WHERE s.OrdersNum = 0 )", null
        )
        val list = ArrayList<StatisticDTO>()
        while (c.moveToNext()) {
            list.add(
                StatisticDTO(
                    c.getString(0),
                    c.getInt(1)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    fun update(context: Context, user: UserDTO) {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        db.execSQL(
            "UPDATE User SET Name = \'${user.name}\', Login = \'${user.login}\', Password = \'${user.password}\' WHERE ID = ${user.id}"
        )
        db.close()
        helper.close()
    }

    fun delete(context: Context, user: UserDTO): Boolean {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        var a = true
        db.beginTransaction()
        try {
            db.execSQL("PRAGMA foreign_keys=ON")
            db.execSQL(
                "DELETE FROM User WHERE ID = ${user.id}"
            )
            db.setTransactionSuccessful()
        } catch (e: Exception) {
            a = false
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a
        }
    }

    fun checkLogin(context: Context, login: String): Boolean {
        if (login.isEmpty()) return false
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val c = db.rawQuery("SELECT * FROM User WHERE Login = \"$login\"", null)
        val a = c.moveToNext()
        c.close()
        db.close()
        helper.close()
        return a
    }

    fun getCompanyUsers(context: Context, idCompany: Long): List<UserDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM User u JOIN Works w ON u.ID = w.UserID WHERE w.CompanyID = $idCompany", null)
        val list = ArrayList<UserDTO>()
        while (c.moveToNext()) {
            list.add(
                UserDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getString(2),
                    c.getString(3)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

}