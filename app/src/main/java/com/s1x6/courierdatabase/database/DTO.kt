package com.s1x6.courierdatabase.database

import java.io.Serializable

data class UserDTO(var id: Long, var name: String, var login: String, var password: String) : Serializable

data class CompanyDTO(var id: Long, var name: String, var balance: Int, var ownerId: Long) : Serializable

data class TransactionDTO(
    var id: Long,
    var sum: Int,
    var balanceAfter: Int,
    var time: Long,
    var companyId: Long,
    var orderId: Long
) : Serializable

data class OrderDTO(
    var id: Long,
    var timeTake: Long,
    var timeDelivery: Long,
    var status: Int,
    var userId: Long,
    var filialId: Long,
    var money: Int,
    var address: String,
    var filial: FilialDTO?,
    var distance: Int
) : Serializable

data class CityDTO(var id: Long, var name: String, var deliveryPrice: Int) : Serializable

data class FilialDTO(var id: Long, var address: String, var companyId: Long, var cityId: Long) : Serializable

data class WorkRelationDTO(var userId: Long, var companyId: Long) : Serializable

data class StatisticDTO(var userName: String, var ordersNum: Int) : Serializable