package com.s1x6.courierdatabase.database.dao

import android.content.Context
import android.database.sqlite.SQLiteStatement
import com.s1x6.courierdatabase.database.CityDTO
import com.s1x6.courierdatabase.database.DBHelper
import java.io.Serializable
import java.lang.Exception

object CityDAO : DAO {
    override fun getAll(context: Context): List<CityDTO> {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        val c = db.rawQuery("SELECT * FROM City", null)
        val list = ArrayList<CityDTO>()
        while (c.moveToNext()) {
            list.add(
                CityDTO(
                    c.getLong(0),
                    c.getString(1),
                    c.getInt(2)
                )
            )
        }
        c.close()
        db.close()
        helper.close()
        return list
    }

    override fun findById(context: Context, id: Long): Serializable? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun add(context: Context, dto: Serializable): Long {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val cityDTO = dto as CityDTO
        db.beginTransaction()
        var a: Long? = null
        try {
            db.execSQL("INSERT INTO City(Name, DeliveryPrice) VALUES(\"${cityDTO.name}\", ${cityDTO.deliveryPrice})")
            val c = db.rawQuery("SELECT max(ID) FROM City", null)
            c.moveToNext()
            a = c.getLong(0)
            c.close()
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a ?: 0L
        }
    }

    fun addMany(context: Context, cities: List<CityDTO>) {
        val helper = DBHelper(context)
        val db = helper.writableDatabase
        val statement = db.compileStatement("INSERT INTO City(Name, DeliveryPrice) VALUES (?, ?)")
        db.beginTransaction()
        try {
            for (i in 0 until cities.size) {
                statement.bindString(1, cities[i].name)
                statement.bindLong(2, cities[i].deliveryPrice.toLong())
                statement.executeInsert()
                statement.clearBindings()
            }
            db.setTransactionSuccessful()
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
        }
    }

    fun update(context: Context, cityDTO: CityDTO) {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        db.execSQL(
            "UPDATE City SET Name = \'${cityDTO.name}\', DeliveryPrice = ${cityDTO.deliveryPrice} WHERE ID = ${cityDTO.id}"
        )
        db.close()
        helper.close()
    }

    fun delete(context: Context, cityDTO: CityDTO): Boolean {
        val helper = DBHelper(context)
        val db = helper.readableDatabase
        var a = true
        db.beginTransaction()
        try {
            db.execSQL("PRAGMA foreign_keys=ON")
            db.execSQL(
                "DELETE FROM City WHERE ID = ${cityDTO.id}"
            )
            db.setTransactionSuccessful()
        } catch (e: Exception) {
            a = false
        } finally {
            db.endTransaction()
            db.close()
            helper.close()
            return a
        }
    }
}