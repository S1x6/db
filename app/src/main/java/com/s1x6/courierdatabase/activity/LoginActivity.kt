package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.UserDAO
import com.s1x6.courierdatabase.database.dao.WorkRelationDAO
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnJoin.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
//        etLogin.setOnKeyListener
        btnLogin.setOnClickListener {
            if (etLogin.text.isEmpty() || etPassword.text.isEmpty()) {
                Toast.makeText(this, "Введите логин и пароль", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val user = UserDAO.getByLogin(this, etLogin.text.toString(), etPassword.text.toString())
            if (user == null) {
                Toast.makeText(this, "Пользователь не найден", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            SharedPreferencesHelper.saveValue(
                this,
                SharedPreferencesHelper.companyId,
                WorkRelationDAO.findCompanies(this, user)[0].id.toString()
            )
            SharedPreferencesHelper.saveValue(this, SharedPreferencesHelper.userId, user.id.toString())
            startActivity(Intent(this, MainActivity::class.java).apply { putExtra("user", user) })
        }
    }
}
