package com.s1x6.courierdatabase.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.FilialDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.CityDAO
import com.s1x6.courierdatabase.database.dao.FilialDAO
import kotlinx.android.synthetic.main.activity_edit_filial.*

class EditFilialActivity : AppCompatActivity() {

    private val selectionCityMap = HashMap<String, Long>()
    private var dataAdapter: ArrayAdapter<String>? = null
    private var filial : FilialDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_filial)
        filial = intent.getSerializableExtra("filial") as FilialDTO
        etAddress.setText(filial?.address)
        dataAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.adapter = dataAdapter
        //loadCities(dataAdapter)
        btnUpdate.setOnClickListener {
            if (etAddress.text.isEmpty()) {
                Toast.makeText(this, "Введите адрес", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val dto = FilialDTO(
                filial?.id ?: 0L,
                etAddress.text.toString(),
                SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong() ?: 0L,
                selectionCityMap[spinnerCity.selectedItem as String] ?: 0L
            )
            FilialDAO.update(this, dto)
            onBackPressed()
        }
        btnDelete.setOnClickListener {
            if (!FilialDAO.delete(this, filial!!)) {
                Toast.makeText(this, "Удаление не удалось. Существуют ссылающиеся сущности", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        loadCities(dataAdapter)
        spinnerCity.setSelection(selectionCityMap.values.indexOf(filial?.cityId)) // ????
    }

    private fun loadCities(dataAdapter: ArrayAdapter<String>?) {
        selectionCityMap.clear()
        val cities = CityDAO.getAll(this)
        cities.forEach { selectionCityMap[it.name] = it.id }
        dataAdapter?.clear()
        dataAdapter?.addAll(selectionCityMap.keys)
        dataAdapter?.notifyDataSetChanged()
    }


}
