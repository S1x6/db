package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.FilialsAdapter
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.FilialDAO
import kotlinx.android.synthetic.main.activity_filials.*

class FilialsActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filials)
        setSupportActionBar(toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.adapter = FilialsAdapter()
        recyclerView?.layoutManager = LinearLayoutManager(this)
//        loadFilials()
    }

    private fun loadFilials() {
        (recyclerView?.adapter as FilialsAdapter?)?.setItems(
            FilialDAO.getCompanyFilials(this, SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong()?:0L)
        )

    }

    override fun onResume() {
        super.onResume()
        loadFilials()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filials_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_add -> startActivity(Intent(this, AddFilialActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
