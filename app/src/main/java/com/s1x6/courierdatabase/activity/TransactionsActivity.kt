package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.TransactionAdapter
import com.s1x6.courierdatabase.database.dao.TransactionDAO
import kotlinx.android.synthetic.main.activity_transactions.*

class TransactionsActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transactions)
        setSupportActionBar(toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        recyclerView?.adapter = TransactionAdapter()
        recyclerView?.layoutManager = LinearLayoutManager(this)
        loadTransactions(0, 10, false)
    }


    override fun onResume() {
        super.onResume()
        loadTransactions(0, 10, false)
    }

    private fun loadTransactions(offset: Int, limit: Int, add: Boolean) {
        val list = TransactionDAO.getPart(this, offset, limit)
//        val list = TransactionDAO.getPart(this, offset, limit)
        if (!add) {
            (recyclerView?.adapter as TransactionAdapter).setItems(list)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filials_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        } else if (item?.itemId == R.id.action_add) {
            startActivity(Intent(this, AddTransactionActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
