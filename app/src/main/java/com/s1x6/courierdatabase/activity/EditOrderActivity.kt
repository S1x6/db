package com.s1x6.courierdatabase.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.OrderDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.CityDAO
import com.s1x6.courierdatabase.database.dao.FilialDAO
import com.s1x6.courierdatabase.database.dao.OrderDAO
import kotlinx.android.synthetic.main.activity_edit_order.*
import java.util.*

@SuppressLint("SetTextI18n")
class EditOrderActivity : AppCompatActivity() {

    private var order: OrderDTO? = null
    private val selectionFilialMap = HashMap<String, Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_order)
        order = intent.getSerializableExtra("dto") as OrderDTO
        etAddress.setText(order?.address)
        val dateTake = Date((order?.timeTake ?: 0) *1000)
        val dateDel = Date((order?.timeDelivery ?: 0) * 1000)
        val timeTakePicker = TimePickerDialog(this, onTakeTimePickedListener, dateTake.hours, dateTake.minutes, true)
        val dateTakePicker =
            DatePickerDialog(this, onTakeDatePickerListener, dateTake.year + 1900, dateTake.month, dateTake.date)
        val dateDeliveryPicker =
            DatePickerDialog(this, onDeliveryDatePickedListener, dateDel.year + 1900, dateDel.month, dateDel.date)
        val timeDeliveryPicker =
            TimePickerDialog(this, onDeliveryTimePicketListener, dateDel.hours, dateDel.minutes, true)

        etTakeDate.isFocusable = false
        etTakeTime.isFocusable = false
        etDeliveryDate.isFocusable = false
        etDeliveryTime.isFocusable = false
        etTakeTime.setOnClickListener { timeTakePicker.show() }
        etTakeDate.setOnClickListener { dateTakePicker.show() }
        etDeliveryTime.setOnClickListener { timeDeliveryPicker.show() }
        etDeliveryDate.setOnClickListener { dateDeliveryPicker.show() }

        etTakeTime.setText(
            "${if (dateTake.hours < 10) {
                "0"
            } else {
                ""
            }}${dateTake.hours}:${if (dateTake.minutes < 10) {
                "0"
            } else {
                ""
            }}${dateTake.minutes}"
        )
        etDeliveryTime.setText(
            "${if (dateDel.hours < 10) {
                "0"
            } else {
                ""
            }}${dateDel.hours}:${if (dateDel.minutes < 10) {
                "0"
            } else {
                ""
            }}${dateDel.minutes}"
        )
        etDeliveryDate.setText(
            "${if (dateDel.date < 10) {
                "0"
            } else {
                ""
            }}${dateDel.date}.${if (dateDel.month+1 < 10) {
                "0"
            } else {
                ""
            }}${dateDel.month+1}.${dateDel.year + 1900}"
        )
        etTakeDate.setText(
            "${if (dateTake.date < 10) {
                "0"
            } else {
                ""
            }}${dateTake.date}.${if (dateTake.month+1 < 10) {
                "0"
            } else {
                ""
            }}${dateTake.month+1}.${dateTake.year + 1900}"
        )

        val dataAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerFilial.adapter = dataAdapter
        loadFilials(dataAdapter)
        etPrice.setText(order?.money.toString())
        etDistance.setText(order?.distance.toString())
        etStatus.setText(order?.status.toString())
        btnUpdate.setOnClickListener {
            if (etAddress.text.isEmpty() || etPrice.text.isEmpty() || etDeliveryTime.text.isEmpty()
                || etDeliveryDate.text.isEmpty() || etTakeDate.text.isEmpty() || etTakeTime.text.isEmpty()
                || etDistance.text.isEmpty()
            ) {
                Toast.makeText(this, "Недостаточно данных", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val takeDateArray = etTakeDate.text.toString().split(".")
            val takeTimeArray = etTakeTime.text.toString().split(":")
            val deliveryDateArray = etDeliveryDate.text.toString().split(".")
            val deliveryTimeArray = etDeliveryTime.text.toString().split(":")
            val takeTime = Date(
                takeDateArray[2].toInt()-1900,
                takeDateArray[1].toInt()-1,
                takeDateArray[0].toInt(),
                takeTimeArray[0].toInt(),
                takeTimeArray[1].toInt()
            )
            val delTime = Date(
                deliveryDateArray[2].toInt()-1900,
                deliveryDateArray[1].toInt()-1,
                deliveryDateArray[0].toInt(),
                deliveryTimeArray[0].toInt(),
                deliveryTimeArray[1].toInt()
            )
            val orderDTO = OrderDTO(
                order?.id ?: 0L,
                takeTime.time/1000,
                delTime.time/1000,
                etStatus.text.toString().toInt(),
                SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.userId)?.toLong() ?: 0L,
                selectionFilialMap[spinnerFilial.selectedItem as String] ?: 0L,
                etPrice.text.toString().toInt(),
                etAddress.text.toString(),
                null,
                etDistance.text.toString().toInt()
            )
            OrderDAO.update(this, orderDTO)
            onBackPressed()
        }
        btnDelete.setOnClickListener {
            if (! OrderDAO.delete(this, order?.id!!)) {
                Toast.makeText(this, "Удаление не удалось. Существуют ссылающиеся сущности", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            onBackPressed()
        }
    }

    private fun loadFilials(dataAdapter: ArrayAdapter<String>) {
        selectionFilialMap.clear()
        val filials = FilialDAO.getCompanyFilials(
            this,
            SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong() ?: 0L
        )
        filials.forEach { selectionFilialMap[it.address] = it.id }
        dataAdapter.clear()
        dataAdapter.addAll(selectionFilialMap.keys)
        dataAdapter.notifyDataSetChanged()
        spinnerFilial.setSelection(selectionFilialMap.values.indexOf(order?.filialId))
    }

    private val onTakeTimePickedListener =
        TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
            etTakeTime.setText(
                "${if (i < 10) {
                    "0"
                } else {
                    ""
                }}$i:${if (i1 < 10) {
                    "0"
                } else {
                    ""
                }}$i1"
            )
        }

    private val onTakeDatePickerListener =
        DatePickerDialog.OnDateSetListener { _: DatePicker, i: Int, i1: Int, i2: Int ->
            etTakeDate.setText(
                "${if (i2 < 10) {
                    "0"
                } else {
                    ""
                }}$i2.${if (i1+1 < 10) {
                    "0"
                } else {
                    ""
                }}${i1+1}.$i"
            )
        }

    private val onDeliveryTimePicketListener =
        TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
            etDeliveryTime.setText(
                "${if (i < 10) {
                    "0"
                } else {
                    ""
                }}$i:${if (i1 < 10) {
                    "0"
                } else {
                    ""
                }}$i1"
            )
        }

    private val onDeliveryDatePickedListener =
        DatePickerDialog.OnDateSetListener { _: DatePicker, i: Int, i1: Int, i2: Int ->
            etDeliveryDate.setText(
                "${if (i2 < 10) {
                    "0"
                } else {
                    ""
                }}$i2.${if (i1+1 < 10) {
                    "0"
                } else {
                    ""
                }}${i1+1}.$i"
            )
        }
}
