package com.s1x6.courierdatabase.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.TransactionDTO
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import com.s1x6.courierdatabase.database.dao.OrderDAO
import com.s1x6.courierdatabase.database.dao.TransactionDAO
import kotlinx.android.synthetic.main.activity_add_transaction.*
import java.util.*

class AddTransactionActivity : AppCompatActivity() {

    private val selectionOrderMap = HashMap<String, Long>()
    private val selectionCompanyMap = HashMap<String, Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_transaction)
        val date = Date(System.currentTimeMillis())
        val timeTakePicker = TimePickerDialog(this, onTakeTimePickedListener, date.hours, date.minutes, true)
        val dateTakePicker = DatePickerDialog(this, onTakeDatePickerListener, date.year + 1900, date.month, date.date)
        etTakeDate.isFocusable = false
        etTakeTime.isFocusable = false
        etTakeTime.setOnClickListener { timeTakePicker.show() }
        etTakeDate.setOnClickListener { dateTakePicker.show() }
        val dataAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        orderSpinner.adapter = dataAdapter
        selectionOrderMap.clear()
        val orders = OrderDAO.getAll(this)
        orders.forEach { selectionOrderMap[it.address] = it.id }
        dataAdapter.clear()
        dataAdapter.addAll(selectionOrderMap.keys)
        dataAdapter.notifyDataSetChanged()
        if (orders.isNotEmpty())
            orderSpinner.setSelection(0)
        val dataAdapter1 = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        companySpinner.adapter = dataAdapter1
        selectionCompanyMap.clear()
        val companies = CompanyDAO.getAll(this)
        companies.forEach { selectionCompanyMap[it.name] = it.id }
        dataAdapter1.clear()
        dataAdapter1.addAll(selectionCompanyMap.keys)
        dataAdapter1.notifyDataSetChanged()
        if (companies.isNotEmpty())
            companySpinner.setSelection(0)

        btnCreate.setOnClickListener {
            if (etSum.text.isEmpty() || etTakeDate.text.isEmpty() || etTakeDate.text.isEmpty() ||
                etBalance.text.isEmpty() || companySpinner.selectedItem == null || orderSpinner.selectedItem == null
            ) {
                Toast.makeText(this, "Недостаточно данных", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val takeDateArray = etTakeDate.text.toString().split(".")
            val takeTimeArray = etTakeTime.text.toString().split(":")
            val takeTime = Date(
                takeDateArray[2].toInt()-1900,
                takeDateArray[1].toInt()-1,
                takeDateArray[0].toInt(),
                takeTimeArray[0].toInt(),
                takeTimeArray[1].toInt()
            )
            val dto = TransactionDTO(
                -1,
                etSum.text.toString().toInt(),
                etBalance.text.toString().toInt(),
                takeTime.time/1000,
                selectionCompanyMap[companySpinner.selectedItem as String]!!,
                selectionOrderMap[orderSpinner.selectedItem as String]!!
            )
            TransactionDAO.add(this, dto)
        }
    }

    @SuppressLint("SetTextI18n")
    private val onTakeTimePickedListener =
        TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
            etTakeTime.setText(
                "${if (i < 10) {
                    "0"
                } else {
                    ""
                }}$i:${if (i1 < 10) {
                    "0"
                } else {
                    ""
                }}$i1"
            )
        }

    @SuppressLint("SetTextI18n")
    private val onTakeDatePickerListener =
        DatePickerDialog.OnDateSetListener { _: DatePicker, i: Int, i1: Int, i2: Int ->
            etTakeDate.setText(
                "${if (i2 < 10) {
                    "0"
                } else {
                    ""
                }}$i2.${if (i1+1 < 10) {
                    "0"
                } else {
                    ""
                }}${i1+1}.$i"
            )
        }

}
