package com.s1x6.courierdatabase.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.UserDTO
import com.s1x6.courierdatabase.database.WorkRelationDTO
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import com.s1x6.courierdatabase.database.dao.UserDAO
import com.s1x6.courierdatabase.database.dao.WorkRelationDAO
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity() {

    private val selectionCompanyMap: MutableMap<String, Long> = HashMap()
    private var dataAdapter: ArrayAdapter<String>? = null
    private var newCompany: CompanyDTO? = null
    private var loginExists = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        etLogin.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (UserDAO.checkLogin(this@SignUpActivity, etLogin.text.toString())) {
                    loginExists = true
                    Toast.makeText(this@SignUpActivity, "Логин \"${etLogin.text}\" занят", Toast.LENGTH_SHORT).show()
                } else {
                    loginExists = false
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        btnAddCompany.setOnClickListener {
            this@SignUpActivity.startActivityForResult(
                Intent(this, AddCompanyActivity::class.java).apply {
                    newCompany?.let { putExtra("dto", it) }
                }, 0
            )
        }

        signUpButton.setOnClickListener {
            if (etLogin.text.isEmpty() || etPassword.text.isEmpty() || etName.text.isEmpty() || companiesSpinner.selectedItem == null) {
                Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (loginExists) {
                Toast.makeText(this, "Логин занят", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val userId = UserDAO.add(
                this,
                UserDTO(0, etName.text.toString(), etLogin.text.toString(), etPassword.text.toString())
            )
            newCompany?.let {
                if (selectionCompanyMap[companiesSpinner.selectedItem as String] == it.id) {
                    it.ownerId = userId
                    it.id = CompanyDAO.add(this, it)
                    selectionCompanyMap[it.name] = it.id
                }
            }
            val companyId = selectionCompanyMap[companiesSpinner.selectedItem as String]!!
            WorkRelationDAO.add(this, WorkRelationDTO(userId, companyId))
            onBackPressed()
            finish()
        }

        dataAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        companiesSpinner.adapter = dataAdapter
        loadCompanies()


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            newCompany = data?.getSerializableExtra("dto") as CompanyDTO
            loadCompanies()
        }
    }

    private fun loadCompanies() {
        selectionCompanyMap.clear()
        val companies = CompanyDAO.getAll(this)
        companies.forEach { selectionCompanyMap[it.name] = it.id }
        newCompany?.apply {
            selectionCompanyMap[name] = id
        }
        dataAdapter?.clear()
        dataAdapter?.addAll(selectionCompanyMap.keys)
        dataAdapter?.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return true
    }
}