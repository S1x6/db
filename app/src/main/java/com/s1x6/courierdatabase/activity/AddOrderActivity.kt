package com.s1x6.courierdatabase.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.OrderDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import com.s1x6.courierdatabase.database.dao.FilialDAO
import com.s1x6.courierdatabase.database.dao.OrderDAO
import kotlinx.android.synthetic.main.activity_add_order.*
import java.util.*

@SuppressLint("SetTextI18n")
class AddOrderActivity : AppCompatActivity() {

    private val selectionFilialMap = HashMap<String, Long>()
    private var price: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_order)
        val date = Date(System.currentTimeMillis())
        val timeTakePicker = TimePickerDialog(this, onTakeTimePickedListener, date.hours, date.minutes, true)
        val dateTakePicker = DatePickerDialog(this, onTakeDatePickerListener, date.year + 1900, date.month, date.date)
        val dateDeliveryPicker =
            DatePickerDialog(this, onDeliveryDatePickedListener, date.year + 1900, date.month, date.date)
        val timeDeliveryPicker = TimePickerDialog(this, onDeliveryTimePicketListener, date.hours, date.minutes, true)
        etTakeDate.isFocusable = false
        etTakeTime.isFocusable = false
        etDeliveryDate.isFocusable = false
        etDeliveryTime.isFocusable = false
        etTakeTime.setOnClickListener { timeTakePicker.show() }
        etTakeDate.setOnClickListener { dateTakePicker.show() }
        etDeliveryTime.setOnClickListener { timeDeliveryPicker.show() }
        etDeliveryDate.setOnClickListener { dateDeliveryPicker.show() }
        val dataAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerFilial.adapter = dataAdapter
        loadFilials(dataAdapter)
        findPrice()
        etDistance.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                calculatePrice()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        spinnerFilial.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                findPrice()
                calculatePrice()
            }

        }
        btnCreate.setOnClickListener {
            if (etAddress.text.isEmpty() || tvPrice.text.isEmpty() || etDeliveryTime.text.isEmpty()
                || etDeliveryDate.text.isEmpty() || etTakeDate.text.isEmpty() || etTakeTime.text.isEmpty()
                || etDistance.text.isEmpty()
            ) {
                Toast.makeText(this, "Недостаточно данных", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val takeDateArray = etTakeDate.text.toString().split(".")
            val takeTimeArray = etTakeTime.text.toString().split(":")
            val deliveryDateArray = etDeliveryDate.text.toString().split(".")
            val deliveryTimeArray = etDeliveryTime.text.toString().split(":")
            val takeTime = Date(
                takeDateArray[2].toInt()-1900,
                takeDateArray[1].toInt()-1,
                takeDateArray[0].toInt(),
                takeTimeArray[0].toInt(),
                takeTimeArray[1].toInt()
            )
            val delTime = Date(
                deliveryDateArray[2].toInt()-1900,
                deliveryDateArray[1].toInt()-1,
                deliveryDateArray[0].toInt(),
                deliveryTimeArray[0].toInt(),
                deliveryTimeArray[1].toInt()
            )
            val orderDTO = OrderDTO(
                0,
                takeTime.time/1000,
                delTime.time/1000,
                0,
                SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.userId)?.toLong() ?: 0L,
                selectionFilialMap[spinnerFilial.selectedItem as String] ?: 0L,
                tvPrice.text.toString().substring(0, tvPrice.text.toString().length - 2).toInt(),
                etAddress.text.toString(),
                null,
                etDistance.text.toString().toInt()
            )
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            OrderDAO.add(this, orderDTO)
            onBackPressed()
        }
    }

    private fun calculatePrice() {
        if (etDistance.text.isEmpty() || price == null) {
            tvPrice.text = ""
            return
        }
        val num = etDistance.text.toString().toLongOrNull()
        num?.let {
            tvPrice.text = (it * price!!).toString() + " Р"
        }

    }

    private fun findPrice() {
        if (selectionFilialMap.isNotEmpty()) {
            price = FilialDAO.getPrice(this, selectionFilialMap[spinnerFilial.selectedItem as String] ?: 0L)
        } else {
            price = null
        }
    }

    private fun loadFilials(dataAdapter: ArrayAdapter<String>) {
        selectionFilialMap.clear()
        val filials = FilialDAO.getCompanyFilials(
            this,
            SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong() ?: 0L
        )
        filials.forEach { selectionFilialMap[it.address] = it.id }
        dataAdapter.clear()
        dataAdapter.addAll(selectionFilialMap.keys)
        dataAdapter.notifyDataSetChanged()
        if (filials.isNotEmpty())
            spinnerFilial.setSelection(0)
    }

    private val onTakeTimePickedListener =
        TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
            etTakeTime.setText(
                "${if (i < 10) {
                    "0"
                } else {
                    ""
                }}$i:${if (i1 < 10) {
                    "0"
                } else {
                    ""
                }}$i1"
            )
        }

    private val onTakeDatePickerListener =
        DatePickerDialog.OnDateSetListener { _: DatePicker, i: Int, i1: Int, i2: Int ->
            etTakeDate.setText(
                "${if (i2 < 10) {
                    "0"
                } else {
                    ""
                }}$i2.${if (i1+1 < 10) {
                    "0"
                } else {
                    ""
                }}${i1+1}.$i"
            )
        }

    private val onDeliveryTimePicketListener =
        TimePickerDialog.OnTimeSetListener { _: TimePicker, i: Int, i1: Int ->
            etDeliveryTime.setText(
                "${if (i < 10) {
                    "0"
                } else {
                    ""
                }}$i:${if (i1 < 10) {
                    "0"
                } else {
                    ""
                }}$i1"
            )
        }

    private val onDeliveryDatePickedListener =
        DatePickerDialog.OnDateSetListener { _: DatePicker, i: Int, i1: Int, i2: Int ->
            etDeliveryDate.setText(
                "${if (i2 < 10) {
                    "0"
                } else {
                    ""
                }}$i2.${if (i1+1 < 10) {
                    "0"
                } else {
                    ""
                }}${i1+1}.$i"
            )
        }

}
