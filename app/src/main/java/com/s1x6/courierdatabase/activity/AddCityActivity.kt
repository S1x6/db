package com.s1x6.courierdatabase.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CityDTO
import com.s1x6.courierdatabase.database.dao.CityDAO
import kotlinx.android.synthetic.main.activity_add_city.*
import kotlinx.android.synthetic.main.activity_edit_order.*

class AddCityActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_city)
        btnCreate.setOnClickListener {
            if (etName.text.isEmpty() || etDeliveryPrice.text.isEmpty()) {
                Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val names = etName.text.toString().split(",")
            names.forEach { it.trim() }
            val prices = etDeliveryPrice.text.toString().split(",")
            prices.forEach {
                if (it.trim().toIntOrNull() == null) {
                    Toast.makeText(this, "Введите числа", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }
            if (names.size != prices.size) {
                Toast.makeText(this, "Количество не совпадает", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val list = ArrayList<CityDTO>()
            for (i in 0 until names.size) {
                list.add(CityDTO(0,names[i].trim(), prices[i].trim().toInt()))
            }
            CityDAO.addMany(this, list)
            onBackPressed()
        }
    }
}
