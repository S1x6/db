package com.s1x6.courierdatabase.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.CompanyInUserAdapter
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.UserDTO
import com.s1x6.courierdatabase.database.WorkRelationDTO
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import com.s1x6.courierdatabase.database.dao.UserDAO
import com.s1x6.courierdatabase.database.dao.WorkRelationDAO
import com.s1x6.courierdatabase.dialog.SwitchCompanyDialog
import kotlinx.android.synthetic.main.activity_edit_user.*

class EditUserActivity : AppCompatActivity(), SwitchCompanyDialog.OnCompanySwitched {

    private val adapter = CompanyInUserAdapter()
    private var user : UserDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_user)
        user = intent.getSerializableExtra("user") as UserDTO
        etName.setText(user?.name)
        etLogin.setText(user?.login)
        etPassword.setText(user?.password)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        btnAddCompany.setOnClickListener {
            SwitchCompanyDialog.newInstance(true).show(supportFragmentManager, "addCompanyDialog")
        }
        btnSave.setOnClickListener {
            if (etName.text.isEmpty() || etLogin.text.isEmpty() || etPassword.text.isEmpty()) {
                Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            user?.name = etName.text.toString()
            user?.login = etLogin.text.toString()
            user?.password = etPassword.text.toString()
            UserDAO.update(this, user!!)
            onBackPressed()
        }

        btnCreateCompany.setOnClickListener {
            startActivityForResult(Intent(this, AddCompanyActivity::class.java), 0)
        }
        btnDelete.setOnClickListener {
            if (!UserDAO.delete(this, user!!)) {
                Toast.makeText(this, "Удаление не удалось. Существуют ссылающиеся сущности", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            val companyDTO = data?.getSerializableExtra("dto") as CompanyDTO
            companyDTO.ownerId = user?.id!!
            val companyId = CompanyDAO.add(this, companyDTO)
            WorkRelationDAO.add(this, WorkRelationDTO(user?.id!!, companyId))
            loadCompanies()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        loadCompanies()
    }

    override fun performAction(companyId: Long) {
        val dto = WorkRelationDTO(
            user!!.id, companyId
        )
        WorkRelationDAO.add(this, dto)
    }

    private fun loadCompanies() {
        adapter.setItems(WorkRelationDAO.findCompanies(this, user!!))
    }
}
