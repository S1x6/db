package com.s1x6.courierdatabase.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CityDTO
import com.s1x6.courierdatabase.database.dao.CityDAO
import kotlinx.android.synthetic.main.activity_edit_city.*

class EditCityActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_city)
        val cityDTO = intent.getSerializableExtra("city") as CityDTO
        etName.setText(cityDTO.name)
        etDeliveryPrice.setText(cityDTO.deliveryPrice.toString())
        btnUpdate.setOnClickListener {
            if (etName.text.isEmpty() || etDeliveryPrice.text.isEmpty()) {
                Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            cityDTO.name = etName.text.toString()
            cityDTO.deliveryPrice = etDeliveryPrice.text.toString().toInt()
            CityDAO.update(this, cityDTO)
            onBackPressed()
        }
        btnDelete.setOnClickListener {
            if (!CityDAO.delete(this, cityDTO)) {
                Toast.makeText(this, "Удаление не удалось. Существуют ссылающиеся сущности", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            onBackPressed()
        }
    }
}
