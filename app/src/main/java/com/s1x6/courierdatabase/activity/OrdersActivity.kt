package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.EndlessRecyclerViewScrollListener
import com.s1x6.courierdatabase.adapter.OrdersAdapter
import com.s1x6.courierdatabase.database.OrderDTO
import com.s1x6.courierdatabase.database.dao.OrderDAO
import com.s1x6.courierdatabase.dialog.OrdersFilterDialog
import com.s1x6.courierdatabase.dialog.OrdersSortDialog
import kotlinx.android.synthetic.main.activity_main.*

class OrdersActivity : AppCompatActivity(), OrdersSortDialog.OnSortOptionSet,
    OrdersFilterDialog.OnOrdersFilteredListener {
    //    private var user: UserDTO? = null
    private var adapter: OrdersAdapter? = null

    private var field = OrdersSortDialog.SortField.ID
    private var dir = OrdersSortDialog.SortDir.ASC
    private var offset = 0
    private var limit = 10
    private var filterOrder = OrderDTO(-1, -1, -1, -1, -1, -1, -1, "", null, -1)

    private val sortDialog = OrdersSortDialog()
    private val filterDialog = OrdersFilterDialog.newInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar2)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = OrdersAdapter()
        recyclerView.adapter = adapter
        recyclerView.setOnScrollListener(object :
            EndlessRecyclerViewScrollListener(recyclerView.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                offset = adapter?.itemCount ?: 0
                adapter?.addOrders(
                    OrderDAO.getSortedPart(
                        this@OrdersActivity, offset, limit,
                        field, dir, filterOrder
                    )
                )
            }

        })
        recyclerView.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
    }

    override fun onFilter(filter: OrderDTO) {
        filterOrder = filter
        offset = 0
        reloadOrders()
    }

    override fun onResume() {
        super.onResume()
        OrderDAO.finishOrders(this)
        offset = 0
        loadOrders(OrdersSortDialog.SortField.ID, OrdersSortDialog.SortDir.ASC)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.orders_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId ) {
            R.id.action_add -> startActivity(Intent(this, AddOrderActivity::class.java))
            R.id.action_sort -> openSortDialog()
            R.id.action_filter -> openFilterDialog()
        }
        return true
    }

    private fun openFilterDialog() {
        filterDialog.show(supportFragmentManager, "filterDialog")
    }

    override fun performAction(field: OrdersSortDialog.SortField, dir: OrdersSortDialog.SortDir) {
//        Toast.makeText(this, "${field.name} ${dir.name}", Toast.LENGTH_SHORT).show()
        offset = 0
        loadOrders(field, dir)
    }

    private fun openSortDialog() {
        sortDialog.show(supportFragmentManager, "sortDialog")
    }

    private fun loadOrders(field: OrdersSortDialog.SortField, dir: OrdersSortDialog.SortDir) {
        this.field = field
        this.dir = dir
        reloadOrders()
    }

    private fun reloadOrders() {
        adapter?.setOrders(OrderDAO.getSortedPart(this, offset, limit, field!!, dir!!, filterOrder))
        adapter?.notifyDataSetChanged()
    }
}
