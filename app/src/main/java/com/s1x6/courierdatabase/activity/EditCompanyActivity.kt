package com.s1x6.courierdatabase.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import kotlinx.android.synthetic.main.activity_edit_company.*

class EditCompanyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_company)
        val company = CompanyDAO.findById(this, SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong()?:0L)
        etName.setText(company?.name)
        etBalance.setText(company?.balance.toString())
        btnUpdate.setOnClickListener {
            if (etName.text.isEmpty() || etBalance.text.isEmpty()) {
                Toast.makeText(this, "Недостаточно данных", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            CompanyDAO.update(this, company!!.apply {
                name = etName.text.toString()
                balance = etBalance.text.toString().toInt()
            })
            onBackPressed()
        }
    }
}
