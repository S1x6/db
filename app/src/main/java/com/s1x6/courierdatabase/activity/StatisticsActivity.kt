package com.s1x6.courierdatabase.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.StatisticsAdapter
import com.s1x6.courierdatabase.database.dao.UserDAO
import kotlinx.android.synthetic.main.activity_statistics.*

class StatisticsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)

        setSupportActionBar(toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = StatisticsAdapter()
        recyclerView.adapter = adapter
        loadStatistic(adapter)
    }

    private fun loadStatistic(adapter: StatisticsAdapter) {
        adapter.setItems(UserDAO.getStatistic(this))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
