package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.FilialDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.CityDAO
import com.s1x6.courierdatabase.database.dao.FilialDAO
import kotlinx.android.synthetic.main.activity_add_filial.*

class AddFilialActivity : AppCompatActivity() {

    private val selectionCityMap = HashMap<String, Long>()
    private var dataAdapter: ArrayAdapter<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_filial)
        dataAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item
        )
        dataAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCity.adapter = dataAdapter

        btnCreate.setOnClickListener {
            if (etAddress.text.isEmpty()) {
                Toast.makeText(this, "Введите адрес", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val dto = FilialDTO(
                0,
                etAddress.text.toString(),
                SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLong() ?: 0L,
                selectionCityMap[spinnerCity.selectedItem as String] ?: 0L
            )
            FilialDAO.add(this, dto)
            onBackPressed()
        }
        btnShowCities.setOnClickListener {
            startActivity(Intent(this, CityActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        loadCities(dataAdapter!!)
    }

    private fun loadCities(dataAdapter: ArrayAdapter<String>) {
        selectionCityMap.clear()
        val cities = CityDAO.getAll(this)
        cities.forEach { selectionCityMap[it.name] = it.id }
        dataAdapter.clear()
        dataAdapter.addAll(selectionCityMap.keys)
        dataAdapter.notifyDataSetChanged()
    }
}
