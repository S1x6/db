
package com.s1x6.courierdatabase.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.s1x6.courierdatabase.adapter.MenuRecyclerAdapter
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.UserDTO
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import com.s1x6.courierdatabase.database.dao.UserDAO
import com.s1x6.courierdatabase.dialog.SwitchCompanyDialog
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity : AppCompatActivity(), SwitchCompanyDialog.OnCompanySwitched {
    private val dialog = SwitchCompanyDialog.newInstance(false)

    private var user: UserDTO? = null
    private var recyclerView: RecyclerView? = null
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar2)
        val companyId =
            SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId)?.toLongOrNull() ?: 0L
        val company = CompanyDAO.findById(this, companyId)
        tvBalance.text = "Баланс: ${company?.balance}"
        supportActionBar?.title = company?.name
        user = intent.getSerializableExtra("user") as UserDTO
        tvUserName.text = user?.name ?: "Инкогнито???"
        setSupportActionBar(toolbar2)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.adapter = MenuRecyclerAdapter(this)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        setMenu()
    }

    override fun onResume() {
        super.onResume()
        user = UserDAO.findById(this, user?.id!!)
        tvUserName.text = user?.name ?: "Инкогнито???"
    }

    private fun setMenu() {
        val currentCompany =
            CompanyDTO(
                (SharedPreferencesHelper.getValue(this, SharedPreferencesHelper.companyId) ?: "0").toLong(),
                "",
                0,
                0
            )
        val userOwner = UserDAO.getCompanyOwner(this, currentCompany)
        (recyclerView?.adapter as MenuRecyclerAdapter?)?.showFilials(userOwner?.id == user?.id)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId ) {
            R.id.action_switch -> dialog.show(supportFragmentManager, "companyDialog")
            R.id.action_edit -> startActivity(Intent(this, EditUserActivity::class.java).apply { putExtra("user", user) })
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    @SuppressLint("SetTextI18n")
    override fun performAction(companyId: Long) {
        SharedPreferencesHelper.saveValue(this, SharedPreferencesHelper.companyId, companyId.toString())
        val company = CompanyDAO.findById(this, companyId)
        supportActionBar?.title = company?.name
        tvBalance.text = "Баланс: ${company?.balance}"
        setMenu()
    }
}
