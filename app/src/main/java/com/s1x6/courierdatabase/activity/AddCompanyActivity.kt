package com.s1x6.courierdatabase.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.CompanyDTO
import kotlinx.android.synthetic.main.activity_add_company.*

class AddCompanyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_company)
        intent?.getSerializableExtra("dto")?.let {
            etName.setText((it as CompanyDTO).name)
        }
        addButton.setOnClickListener {
            setResult(
                Activity.RESULT_OK,
                Intent().also { it.putExtra("dto", CompanyDTO(-1, etName.text.toString(), 0, 0)) })
            onBackPressed()
            finish()
        }
    }
}