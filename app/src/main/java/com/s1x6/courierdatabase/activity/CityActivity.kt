package com.s1x6.courierdatabase.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.CityAdapter
import com.s1x6.courierdatabase.database.dao.CityDAO
import kotlinx.android.synthetic.main.activity_city.*

class CityActivity : AppCompatActivity() {


    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city)
        setSupportActionBar(toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.adapter = CityAdapter()
        recyclerView?.layoutManager = LinearLayoutManager(this)
//        loadCities()
    }

    private fun loadCities() {
        (recyclerView?.adapter as CityAdapter?)?.setItems(
            CityDAO.getAll(this)
        )
    }

    override fun onResume() {
        super.onResume()
        loadCities()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_add -> startActivity(Intent(this, AddCityActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filials_menu, menu)
        return true
    }
}
