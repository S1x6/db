package com.s1x6.courierdatabase.dialog

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.s1x6.courierdatabase.R
import kotlinx.android.synthetic.main.fragment_sort_dialog.*

class OrdersSortDialog : DialogFragment() {

    private var listener : OnSortOptionSet? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_sort_dialog, container)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnApply.setOnClickListener {
            if (rbId.isChecked) {
                listener?.performAction(SortField.ID, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbTimeTake.isChecked) {
                listener?.performAction(SortField.TimeTake, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbTimeDelivery.isChecked) {
                listener?.performAction(SortField.TimeDelivery, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbStatus.isChecked) {
                listener?.performAction(SortField.Status, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbUser.isChecked) {
                listener?.performAction(SortField.UserID, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbMoney.isChecked) {
                listener?.performAction(SortField.Money, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbAddress.isChecked) {
                listener?.performAction(SortField.Address, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbDistance.isChecked) {
                listener?.performAction(SortField.Distance, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            if (rbFilial.isChecked) {
                listener?.performAction(SortField.FilialID, if (rbDesc.isChecked) SortDir.DESC else SortDir.ASC)
            }
            dismiss()
        }

    }

    override fun onAttach(activity: Activity?) {
        if (activity is OnSortOptionSet) {
            listener = activity
        }
        super.onAttach(activity)
    }

    override fun onDetach() {
        listener = null
        super.onDetach()
    }

    enum class SortField {
        ID, TimeTake, TimeDelivery, Status, UserID, Money, Address, Distance, FilialID
    }

    enum class SortDir {
        DESC, ASC
    }

    interface OnSortOptionSet {
        fun performAction(fiald: SortField, dir: SortDir)
    }
}