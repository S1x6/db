package com.s1x6.courierdatabase.dialog

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.database.OrderDTO
import com.s1x6.courierdatabase.database.SharedPreferencesHelper
import com.s1x6.courierdatabase.database.dao.FilialDAO
import com.s1x6.courierdatabase.database.dao.UserDAO
import kotlinx.android.synthetic.main.fragment_orders_filter_dialog.*
import java.util.*

class OrdersFilterDialog : DialogFragment() {

    private var listener: OnOrdersFilteredListener? = null
    private val selectionFilialMap = HashMap<String, Long>()
    private val selectionUserMap = HashMap<String, Long>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectionFilialMap.clear()
        val dataAdapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_spinner_item
        )
        val filials = FilialDAO.getCompanyFilials(
            requireContext(),
            SharedPreferencesHelper.getValue(requireContext(), SharedPreferencesHelper.companyId)?.toLong() ?: 0L
        )
        selectionFilialMap[""] = -1
        filials.forEach { selectionFilialMap[it.address] = it.id }
        dataAdapter.clear()
        //dataAdapter.add("")
        dataAdapter.addAll(selectionFilialMap.keys)
        dataAdapter.notifyDataSetChanged()
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerFilial.adapter = dataAdapter
        spinnerFilial.setSelection(0)

        val dataAdapter1 = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_spinner_item
        )
        val users = UserDAO.getCompanyUsers(
            requireContext(),
            SharedPreferencesHelper.getValue(requireContext(), SharedPreferencesHelper.companyId)?.toLong() ?: 0L
        )
        selectionUserMap[""] = -1
        users.forEach { selectionUserMap[it.name] = it.id }
        dataAdapter1.clear()
       // dataAdapter1.add("")
        dataAdapter1.addAll(selectionUserMap.keys)
        dataAdapter1.notifyDataSetChanged()
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerUser.adapter = dataAdapter1
        spinnerUser.setSelection(0)
        btnFilter.setOnClickListener {
            val order = OrderDTO(
                etId.text.let {
                    if (it.isEmpty()) {
                        -1L
                    } else {
                        it.toString().toLong()
                    }
                },
                0,
                0,
                etStatus.text.let {
                    if (it.isEmpty()) {
                        -1
                    } else {
                        it.toString().toInt()
                    }
                },
                selectionUserMap[spinnerUser.selectedItem as String]!!,
                selectionFilialMap[spinnerFilial.selectedItem as String]!!,
                etPrice.text.let {
                    if (it.isEmpty()) {
                        -1
                    } else {
                        it.toString().toInt()
                    }
                },
                etAddress.text.toString(),
                null,
                etDistance.text.let {
                    if (it.isEmpty()) {
                        -1
                    } else {
                        it.toString().toInt()
                    }
                }
            )
            listener?.onFilter(order)
            dismiss()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders_filter_dialog, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnOrdersFilteredListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnOrdersFilteredListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnOrdersFilteredListener {
        fun onFilter(filter: OrderDTO)
    }

    companion object {
        @JvmStatic
        fun newInstance() = OrdersFilterDialog()
    }
}
