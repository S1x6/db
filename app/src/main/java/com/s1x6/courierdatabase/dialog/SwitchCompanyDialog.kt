package com.s1x6.courierdatabase.dialog

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.s1x6.courierdatabase.R
import com.s1x6.courierdatabase.adapter.CompaniesAdapter
import com.s1x6.courierdatabase.database.CompanyDTO
import com.s1x6.courierdatabase.database.dao.CompanyDAO
import kotlinx.android.synthetic.main.fragment_sort_dialog.*

class SwitchCompanyDialog : DialogFragment() {

    private var listener : OnCompanySwitched? = null
    private var recyclerView: RecyclerView? = null
    private var loadAll: Boolean? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.company_switch_fragment, container)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView?.layoutManager = LinearLayoutManager(requireContext())
        recyclerView?.adapter = CompaniesAdapter(object : CompaniesAdapter.OnCompanyItemClickListener{
            override fun performAction(companyDTO: CompanyDTO) {
                listener?.performAction(companyDTO.id)
                dismiss()
            }
        })
        if (!loadAll!!) {
            (recyclerView?.adapter as CompaniesAdapter?)?.setItems(CompanyDAO.getUsersCompanies(requireContext()))
        } else {
            (recyclerView?.adapter as CompaniesAdapter?)?.setItems(CompanyDAO.getRestCompanies(requireContext()))
        }
    }

    override fun onAttach(activity: Activity?) {
        if (activity is OnCompanySwitched) {
            listener = activity
        }
        super.onAttach(activity)
    }

    override fun onDetach() {
        listener = null
        super.onDetach()
    }

    enum class SortField {
        ID, TimeTake, TimeDelivery, Status, UserID, Money, Address, Distance, FilialID
    }

    enum class SortDir {
        DESC, ASC
    }

    interface OnCompanySwitched {
        fun performAction(companyId: Long)
    }

    companion object {
        fun newInstance(loadAll: Boolean): SwitchCompanyDialog {
            val dialog = SwitchCompanyDialog()
            dialog.loadAll = loadAll
            return dialog
        }
    }
}